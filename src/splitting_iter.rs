use cow_appender::CowAppender;
use std::borrow::Cow;

/// Splits a given string at given separator, skipping first `begin` chars while skipping escaped
/// separator (`\char`).
///
/// If the separator was found, the function returns a part of string after the `begin` and before
/// the separator (separator not included) and the position of the slash.
///
/// If the separator was not found, the whole string starting from the `begin` is returned (with
/// escaped slashes being unescaped), and the position equals to the string length.
///
/// Please note that escape characters not followed by slashes are kept unchanged.
fn split_escape<'a>(string: &'a str, begin: usize, sep: char) -> (Cow<'a, str>, usize) {
    let mut buf = CowAppender::new(string, begin);
    let mut shift = begin;
    while shift < string.len() {
        match find_at(&string, shift, sep) {
            Some(pos) => {
                if let Some('\\') = get_previous_char(string, pos) {
                    // Escaped slash found. Need to skip the character before it.
                    buf.append(shift, pos - 1);
                    buf.push(pos);
                    shift = pos + 1;
                } else {
                    // Unescaped slash found.
                    buf.append(shift, pos);
                    return (buf.into_inner(), pos);
                }
            }
            None => break,
        }
    }
    buf.append(shift, string.len());
    (buf.into_inner(), string.len())
}

/// Detect the separator automatically
fn detect_separator(string: &str) -> char {
    string.chars().skip_while(char::is_ascii_alphabetic).next().unwrap_or('/')
}

/// Gets a previous character relatively to the `pos` or `None` if it is out of bounds.
fn get_previous_char(string: &str, pos: usize) -> Option<char> {
    if pos == 0 {
        None
    } else {
        string.chars().nth(pos - 1)
    }
}

/// Finds a character starting at a given `begin` position.
///
/// # Panics
///
/// Will panic if `begin` is out of bounds of the given string.
fn find_at(string: &str, begin: usize, c: char) -> Option<usize> {
    let (_, part) = string.split_at(begin);
    part.find(c).map(|pos| pos + begin)
}

/// An iterator over splitted parts of a string.
pub struct SlashSplitIter<'a> {
    original: &'a str,
    position: usize,
    separator: char,
}

impl<'a> From<&'a str> for SlashSplitIter<'a> {
    fn from(string: &'a str) -> Self {
        SlashSplitIter {
            original: string,
            position: 0,
            separator: detect_separator(string),
        }
    }
}

impl<'a> Iterator for SlashSplitIter<'a> {
    type Item = Cow<'a, str>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.position >= self.original.len() {
            return None;
        }
        let (part, pos) = split_escape(self.original, self.position, self.separator);
        self.position = match pos.checked_add(1) {
            Some(x) => x,
            None => self.original.len(),
        };
        Some(part)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn find_separator() {
        assert_eq!('/', detect_separator("s/foo/bar"));
        assert_eq!('#', detect_separator("s#foo#bar"));
        assert_eq!('/', detect_separator("foo"));
    }

    #[test]
    fn split_first_empty() {
        let data = "/something/";
        assert_eq!(("".into(), 0), split_escape(data, 0, '/'));
    }

    #[test]
    fn split_once() {
        let data = r"s/ahaha\/lol/wut/g";
        assert_eq!(("s".into(), 1), split_escape(data, 0, '/'));
        assert_eq!(("".into(), 1), split_escape(data, 1, '/'));
        assert_eq!(("ahaha/lol".into(), 12), split_escape(data, 2, '/'));
        assert_eq!(("haha/lol".into(), 12), split_escape(data, 3, '/'));
        assert_eq!(("".into(), 16), split_escape(data, 16, '/'));
        assert_eq!(("g".into(), 18), split_escape(data, 17, '/'));
    }

    #[test]
    fn split_once_last() {
        assert_eq!(("last/".into(), 6), split_escape(r"last\//", 0, '/'));
    }

    #[test]
    fn iter() {
        let data = r"a/fir\/st/second/th\/ird/fo\/rth/last";
        let mut iter = SlashSplitIter::from(data);
        assert_eq!(Some("a".into()), iter.next());
        assert_eq!(Some("fir/st".into()), iter.next());
        assert_eq!(Some("second".into()), iter.next());
        assert_eq!(Some("th/ird".into()), iter.next());
        assert_eq!(Some("fo/rth".into()), iter.next());
        assert_eq!(Some("last".into()), iter.next());
        assert_eq!(None, iter.next());
    }

    #[test]
    fn iter_first_empty() {
        let data = r"/fir\/st/second/th\/ird/fo\/rth/last";
        let mut iter = SlashSplitIter::from(data);
        assert_eq!(Some("".into()), iter.next());
        assert_eq!(Some("fir/st".into()), iter.next());
        assert_eq!(Some("second".into()), iter.next());
        assert_eq!(Some("th/ird".into()), iter.next());
        assert_eq!(Some("fo/rth".into()), iter.next());
        assert_eq!(Some("last".into()), iter.next());
        assert_eq!(None, iter.next());
    }

    #[test]
    fn iter_last_empty() {
        let data = "something/last/";
        let mut iter = SlashSplitIter::from(data);
        assert_eq!(Some("something".into()), iter.next());
        assert_eq!(Some("last".into()), iter.next());
        assert_eq!(None, iter.next());

        let data = r"something/last\/";
        let mut iter = SlashSplitIter::from(data);
        assert_eq!(Some("something".into()), iter.next());
        assert_eq!(Some("last/".into()), iter.next());
        assert_eq!(None, iter.next());
    }
}
