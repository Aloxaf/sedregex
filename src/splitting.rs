use command::Command;
use regex::{Regex, RegexBuilder};
use regex_flags::RegexFlags;
use splitting_iter::SlashSplitIter;
use std::borrow::Cow;
use ErrorKind;

/// A structure holding all the data to execute a regular expression substitution.
#[derive(Debug, PartialEq)]
pub struct ReplaceData<'a> {
    pattern: Cow<'a, str>,
    pub with: Cow<'a, str>,
    pub flags: RegexFlags,
}

impl<'a> ReplaceData<'a> {
    pub fn build_regex(&self) -> Result<Regex, ErrorKind> {
        let mut builder = RegexBuilder::new(&self.pattern);
        self.flags.apply(&mut builder);
        builder.build().map_err(ErrorKind::RegexError)
    }
}

/// Splits a given string into a `ReplaceData` object.
pub fn split_for_replace(data: &str) -> Result<ReplaceData, ErrorKind> {
    let mut it = SlashSplitIter::from(data);
    let _command: Command = match it.next() {
        Some(cmd) => cmd.parse()?,
        None => return Err(ErrorKind::NotEnoughSegments),
    };
    let pattern = it.next().ok_or_else(|| ErrorKind::NotEnoughSegments)?;
    let with = it.next().ok_or_else(|| ErrorKind::NotEnoughSegments)?;
    let flags = match it.next() {
        Some(s) => s.parse()?,
        None => RegexFlags::default(),
    };
    Ok(ReplaceData {
        pattern,
        with,
        flags,
    })
}

#[cfg(test)]
mod tests {
    use super::*;
    use regex_flags::RegexFlag;

    #[test]
    fn global_insensitive() {
        let data = r"s/ahahaha/lol\/wut/gi";
        assert_eq!(
            Ok(ReplaceData {
                pattern: "ahahaha".into(),
                with: "lol/wut".into(),
                flags: vec!(RegexFlag::Global, RegexFlag::CaseInsensitive).into(),
            }),
            split_for_replace(data),
        );
    }

    #[test]
    fn corner() {
        let cmd = r"s/lol/wut\//gi";
        assert_eq!(
            Ok(ReplaceData {
                pattern: "lol".into(),
                with: r"wut/".into(),
                flags: vec!(RegexFlag::CaseInsensitive, RegexFlag::Global).into()
            }),
            split_for_replace(cmd)
        );
    }

    #[test]
    fn no_flags() {
        let data = r"s/aha\/haha/lolwut/";
        assert_eq!(
            Ok(ReplaceData {
                pattern: "aha/haha".into(),
                with: "lolwut".into(),
                flags: vec!().into(),
            }),
            split_for_replace(data),
        );
    }

    #[test]
    fn no_flags2() {
        let data = r"s/aha\/haha/lolwut";
        assert_eq!(
            Ok(ReplaceData {
                pattern: "aha/haha".into(),
                with: "lolwut".into(),
                flags: vec!().into(),
            }),
            split_for_replace(data),
        );
    }

    #[test]
    fn empty_with() {
        let data = r"s/aha\/haha/";
        assert_eq!(Err(ErrorKind::NotEnoughSegments), split_for_replace(data),);
    }

    #[test]
    fn unknown_command() {
        let data = "wtf/what/with/";
        assert_eq!(
            Err(ErrorKind::UnknownCommand("wtf".into())),
            split_for_replace(data),
        );
    }

    #[test]
    fn unknown_flag() {
        let data = "s/what/with/u";
        assert_eq!(Err(ErrorKind::UnknownFlag('u')), split_for_replace(data),);
    }
}
